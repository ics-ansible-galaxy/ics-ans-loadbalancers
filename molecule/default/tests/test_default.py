import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('loadbalancers')


def test_keepalived(host):
    # TODO: implement at least a test
    service = host.service("keepalived")
    assert service.is_running
    assert service.is_enabled


def test_haproxy(host):
    # TODO: implement at least a test
    service = host.service("haproxy")
    assert service.is_running
    assert service.is_enabled
